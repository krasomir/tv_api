# TvMovies API TMDB

## Install

For install follow this steps:

- Clone to your local environment
- Set your database information in .env file
- Add this to your .env file:

```php
    - TMDB_KEY={YOUR_TMDB_API_KEY}  
    - TMDB_BASE_URL=https://api.themoviedb.org/3/  
    - TMDB_IMAGE_BASE_URL=https://image.tmdb.org/t/p/  
    - L5_SWAGGER_CONST_HOST=http://localhost/tv_api/public/api  
```

- Run this commands:

```bash
    cd your_project_name                # go to your project folder  
    $ composer install                  # install dependencies  
    $ php artisan migrate --seed        # run migrations and seed the data  
    $ php artisan key:generate          # generate laravel key  
```

To get your TMDB API key go to [TMDB API](https://www.themoviedb.org/documentation/api).  

Now you can start searching for movies or TV shows.

## Swagger documentation  

To create API documentation run this commands:

```sh
    cd your_project_name                    # go to your project folder  
    $ php artisan l5-swagger:generate       # to generate documentation  
```

To see API documentation go to `/api/documentation`  

To see project in action go to [TvMovie API TMDB](https://tvapi.rentajme.com.hr/) & [API documentation](https://tvapi.rentajme.com.hr/api/documentation)  

***
