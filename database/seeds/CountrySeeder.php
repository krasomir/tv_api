<?php

use App\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->truncate();

        $countries = Http::get('https://api.themoviedb.org/3/configuration/countries?api_key=' . env('TMDB_KEY'))->body();

        foreach (json_decode($countries) as $key => $country) {
            Country::create([
                'iso_3166_1' => $country->iso_3166_1,
                'english_name' => $country->english_name
            ]);
        }
    }
}
