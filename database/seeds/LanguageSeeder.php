<?php

use App\Language;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->truncate();

        $languages = Http::get('https://api.themoviedb.org/3/configuration/languages?api_key=' . env('TMDB_KEY'))->body();

        foreach (json_decode($languages) as $key => $language) {
            Language::create([
                'iso_639_1' => $language->iso_639_1,
                'english_name' => $language->english_name,
                'name' => $language->name
            ]);
        }
    }
}
