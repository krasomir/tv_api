<?php

use App\Genre;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('genres')->truncate();

        $tvGenres = Http::get('https://api.themoviedb.org/3/genre/tv/list?api_key=' . env('TMDB_KEY'))->body();
        $movieGenres = Http::get('https://api.themoviedb.org/3/genre/movie/list?api_key=' . env('TMDB_KEY'))->body();

        $tvGenres = json_decode($tvGenres, true)['genres'];
        $movieGenres = json_decode($movieGenres, true)['genres'];
        $allGenres = array_merge($tvGenres, $movieGenres);
        $tempArr = array_unique(array_column($allGenres, 'name'));
        $uniqueGenres = array_intersect_key($allGenres, $tempArr);

        foreach ($uniqueGenres as $key => $genre) {
            Genre::create([
                'id' => $genre['id'],
                'name' => $genre['name']
            ]);
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
