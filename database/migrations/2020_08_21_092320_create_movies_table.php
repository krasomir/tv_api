<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->id();
            $table->decimal('popularity', 10, 5);
            $table->boolean('video')->default(false);
            $table->integer('vote_count');
            $table->decimal('vote_average', 3, 1);
            $table->string('title')->nullable();
            $table->date('release_date')->nullable();
            $table->string('original_language')->nullable();
            $table->string('original_title')->nullable();
            $table->string('backdrop_path')->nullable();
            $table->boolean('adult')->default(false);
            $table->text('overview')->nullable();
            $table->string('poster_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
