<?php

namespace App;

use App\Genre;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     type="array",
 *     @OA\Items(
 *         @OA\Xml(name="Movie"),
 *         @OA\Property(property="popularity", type="number", readOnly="true", example="356.421"),
 *         @OA\Property(property="video", type="integer", readOnly="true", example="0"),
 *         @OA\Property(property="vote_count", type="number", example="39687"),
 *         @OA\Property(property="vote_average", type="number", example="6.9"),
 *         @OA\Property(property="title", type="string", example="Top Gun"),
 *         @OA\Property(property="release_date", type="string", format="date", example="2020-10-10"),
 *         @OA\Property(property="original_language", type="string", example="hr"),
 *         @OA\Property(property="original_title", type="string", example="Top Gun"),
 *         @OA\Property(property="backdrop_path", type="string", example="/am0wHjplG1YynuDWz2LPWx5V9kJ.jpg"),
 *         @OA\Property(property="adult", type="integer", example="0"),
 *         @OA\Property(property="overview", type="string", example="For Lieutenant Pete 'Maverick' Mitchell and his friend and co-pilot Nick 'Goose' Bradshaw, being accepted into an elite training school for fighter pilots is a dream come true. But a tragedy, as well as personal demons, will threaten Pete's dreams of becoming an ace pilot."),
 *         @OA\Property(property="poster_path", type="string", example="/xUuHj3CgmZQ9P2cMaqQs4J0d4Zc.jpg"),
 *         @OA\Property(property="genre_ids", type="array", 
 *            @OA\Items(
 *               type="integer", example=18
 *            )
 *         ),
 *         @OA\Property(property="created_at", type="string", format="date-time", description="Initial creation timestamp", readOnly="true"),
 *         @OA\Property(property="updated_at", type="string", format="date-time", description="Last update timestamp", readOnly="true")
 *     )
 * )
 *
 * Class Movie
 */
class Movie extends Model
{
    public $fillable = [
        'id', 
        'popularity', 
        'video', 
        'vote_count', 
        'vote_average', 
        'title', 
        'release_date', 
        'original_language', 
        'original_title', 
        'backdrop_path', 
        'adult', 
        'overview', 
        'poster_path'
    ];

    public function genres()
    {
        return $this->belongsToMany(Genre::class);
    }

    /**
     * Get formated release date.
     *
     * @param  string  $value
     * @return string
     */
    public function getReleaseDateAttribute($value)
    {
        return Carbon::parse($value)->format('d.m.Y.');
    }

    /**
     * Undocumented function
     *
     * @param array $result
     * @return boolean
     */
    public function saveMovie(array $result)
    {        
        $this->popularity = isset($result['popularity']) ? $result['popularity'] : 0.0;
        $this->video = isset($result['video']) ? $result['video'] : 0;
        $this->vote_count = $result['vote_count'];
        $this->poster_path = $result['poster_path'];
        if (isset($result['id'])) {
            $this->id = $result['id'];
        }        
        $this->adult = $result['adult'];
        $this->backdrop_path = $result['backdrop_path'];
        $this->original_language = $result['original_language'];
        $this->original_title = $result['original_title'];
        $this->title = $result['title'];
        $this->vote_average = $result['vote_average'];
        $this->overview = $result['overview'];
        $this->release_date = (isset($result['release_date'])) ? $result['release_date'] : null;

        return $this->save();          
    }
}
