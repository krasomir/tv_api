<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MovieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'popularity' => $this->popularity,
            'vote_count' => $this->vote_count,
            'video' => (bool) $this->video,
            'poster_path' => $this->poster_path,
            'id' => $this->id,
            'adult' => (bool) $this->adult,
            'backdrop_path' => $this->backdrop_path,
            'original_language' => $this->original_language,
            'original_title' => $this->original_title,
            'title' => $this->title,
            'vote_average' => $this->vote_average,
            'overview' => $this->overview,
            'release_date' => $this->release_date,
            'genres' => $this->genres,
        ];
    }
}
