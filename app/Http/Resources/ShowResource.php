<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'original_name' => $this->original_name,
            'name' => $this->name,
            'popularity' => $this->popularity,
            'origin_country' => $this->origin_country,
            'vote_count' => $this->vote_count,
            'first_air_date' => $this->first_air_date,
            'backdrop_path' => $this->backdrop_path,
            'original_language' => $this->original_language,
            'id' => $this->id,
            'vote_average' => $this->vote_average,
            'overview' => $this->overview,
            'poster_path' => $this->poster_path,            
            "genre_ids" => $this->genres,
        ];
    }
}
