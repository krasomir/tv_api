<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ShowController extends Controller
{
    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return view
     */
    public function showDetails($id)
    {
        $apiUrl = route('get-show', ['id' => $id]);
        $searchResult = Http::get($apiUrl)->body();
        $movie = json_decode($searchResult)->data;

        $tmdbDetails = Http::get(env('TMDB_BASE_URL') .'tv/' . $id . '?api_key=' . env('TMDB_KEY'))->body();
        $tmdbCredits = Http::get(env('TMDB_BASE_URL') .'tv/' . $id . '/credits?api_key=' . env('TMDB_KEY'))->body();

        return view('show-details', [
            'show' => $movie,
            'details' => json_decode($tmdbDetails),
            'credits' => json_decode($tmdbCredits),
        ]);
    }
}
