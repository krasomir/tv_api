<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Http\Requests\SearchRequest;
use Illuminate\Support\Facades\Http;

class SearchController extends Controller
{
    /**
     * Undocumented function
     *
     * @return view
     */
    public function index()
    {
        return view('welcome', [
            'searchResult' => [],
        ]);
    }

    /**
     * Undocumented function
     *
     * @param SearchRequest $request
     * @return view
     */
    public function search(SearchRequest $request)
    {
        $name = $request->searchTerm;
        $searchType = $request->searchType;
        $decodedResult = $this->getMovieOrShow($request);

        if(empty($decodedResult)) {
            $tmdbResult = $this->searchTmdb($name, $searchType);
            if(!empty($tmdbResult)) {
                $saved = $this->saveTmdbResult($tmdbResult, $searchType);

                if ($saved) {
                    $decodedResult = $this->getMovieOrShow($request);
                }                
            }
        }

        return view('welcome', [
            'searchResult' => $decodedResult,
            'searchType' => $searchType,
        ]);
    }

    /**
     * Undocumented function
     *
     * @param SearchRequest $request
     * @return void
     */
    public function getMovieOrShow(SearchRequest $request)
    {
        $name = $request->searchTerm;
        $searchType = $request->searchType;
        $apiUrl = ($searchType === 'movie') ? 
            route('get-movies', ['name' => $name]) : 
            route('get-shows', ['name' => $name]);

        $searchResult = Http::get($apiUrl)->body();

        return json_decode($searchResult)->data;
    }

    /**
     * Undocumented function
     *
     * @param string $name
     * @param string $type
     * @return void
     */
    public function searchTmdb(string $name, string $type)
    {
        $urlBasePart = env('TMDB_BASE_URL');
        $urlSearchPart = 'search/' . $type . '/?api_key=' . env('TMDB_KEY') . '&query=' . $name;
        $tmdb = Http::get($urlBasePart . $urlSearchPart)->body();

        return json_decode($tmdb)->results;
    }

    /**
     * Undocumented function
     *
     * @param [type] $result
     * @param [type] $type
     * @return boolean|Exception
     */
    public function saveTmdbResult($result, $type)
    {
        $apiUrl = ($type === 'movie') ? 
            route('save-movie') : 
            route('save-show');

        try {
            $saved = Http::post($apiUrl, $result);
            return $saved;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        
    }
}
