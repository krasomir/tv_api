<?php

namespace App\Http\Controllers\Api;

class Controller
{
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="TvMovie TMDB OpenApi Documentation",
     *      description="L5 Swagger OpenApi description",
     *      @OA\Contact(
     *          email="kmioc@inet.hr"
     *      ),
     *      @OA\License(
     *          name="Apache 2.0",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      )
     * )
     *
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="TvMovie TMDB API Server"
     * )
     *
     *
     * @OA\Tag(
     *     name="TvMovieAPI TMDB",
     *     description="API Endpoints of TvMovie TMDB"
     * )
     */
}
