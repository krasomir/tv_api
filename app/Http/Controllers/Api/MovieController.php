<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MovieResource;
use App\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    /**
     * @OA\Get(
     *      path="/movies/{name}",
     *      tags={"Movie"},
     *      summary="Get list of movies",
     *      description="Returns list of movies",
     *      @OA\Parameter(
     *          name="name",
     *          description="movie name",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Movie")
     *      )
     * )
     */
    public function index(string $name = null)
    {

        $movies = ($name) ? Movie::where('title', 'like', '%'. $name .'%')->with('genres')->get() : Movie::paginate(10);

        return MovieResource::collection($movies);
    }

    /**
     * @OA\Post(
     *      path="/movie",
     *      tags={"Movie"},
     *      summary="Store new movie",
     *      description="Returns movie data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Movie")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Movie")
     *      )
     * )
     */
    public function store(Request $request)
    {
        $resources = [];
        foreach ($request->all() as $result) {

            $movie = new Movie;
            $savedMovie = $movie->saveMovie($result);
            
            if ($savedMovie) {
                $movie->genres()->sync($result['genre_ids']);
                $resources[] = new MovieResource($movie);
            }
        }
        return $resources;
    }

    /**
     * @OA\Get(
     *      path="/movie/{id}",
     *      tags={"Movie"},
     *      summary="Get movie information",
     *      description="Returns movie data",
     *      @OA\Parameter(
     *          name="id",
     *          description="movie id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Movie")
     *      )
     * )
     */
    public function show($id)
    {
        $movies = Movie::findOrFail($id);

        return new MovieResource($movies);
    }
}
