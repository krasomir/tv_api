<?php

namespace App\Http\Controllers\Api;

use App\Show;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ShowResource;

class ShowController extends Controller
{
    /**
     * @OA\Get(
     *      path="/shows/{name}",
     *      tags={"Show"},
     *      summary="Get list of TV shows",
     *      description="Returns list of TV shows",
     *      @OA\Parameter(
     *          name="name",
     *          description="TV show name",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Show")
     *      )
     * )
     */
    public function index(string $name = null)
    {
        $shows = ($name) ? Show::where('name', 'like', '%'. $name .'%')->get() : Show::paginate(10);

        return ShowResource::collection($shows);
    }

    /**
     * @OA\Post(
     *      path="/show",
     *      tags={"Show"},
     *      summary="Store new TV show",
     *      description="Returns TV show data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Show")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Show")
     *      )
     * )
     */
    public function store(Request $request)
    {
        $resources = [];
        foreach ($request->all() as $result) {

            $show = new Show;
            $savedShow = $show->saveShow($result);

            if ($savedShow) {
                $show->genres()->sync($result['genre_ids']);
                $resources[] = new ShowResource($show);
            }
        }
        return $resources;
    }

    /**
     * @OA\Get(
     *      path="/show/{id}",
     *      tags={"Show"},
     *      summary="Get TV show information",
     *      description="Returns TV show data",
     *      @OA\Parameter(
     *          name="id",
     *          description="show id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/Show")
     *      )
     * )
     */
    public function show($id)
    {
        $show = Show::findOrFail($id);

        return new ShowResource($show);
    }
}
