<?php

namespace App;

use App\Genre;
use App\Language;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     type="array",
 *     @OA\Items(
 *         @OA\Xml(name="Show"),
 *         @OA\Property(property="id", type="number", readOnly="true", example="1668"),
 *         @OA\Property(property="popularity", type="number", readOnly="true", example="356.421"),
 *         @OA\Property(property="vote_count", type="number", example="39687"),
 *         @OA\Property(property="vote_average", type="number", example="6.9"),
 *         @OA\Property(property="name", type="string", example="Friends"),
 *         @OA\Property(property="original_language", type="string", example="hr"),
 *         @OA\Property(property="backdrop_path", type="string", example="/l0qVZIpXtIo7km9u5Yqh0nKPOr5.jpg"),
 *         @OA\Property(property="overview", type="string", example="The misadventures of a group of friends as they navigate the pitfalls of work, life and love in Manhattan."),
 *         @OA\Property(property="poster_path", type="string", example="/f496cm9enuEsZkSPzCwnTESEK5s.jpg"),
 *         @OA\Property(property="original_name", type="string", example="Friends"),
 *         @OA\Property(property="origin_country", type="integer", readOnly="true", example="17"),
 *         @OA\Property(property="first_air_date", type="string", format="date", example="2020-10-10"),
 *         @OA\Property(property="genre_ids", type="array", 
 *            @OA\Items(
 *               type="integer", example=28
 *            )
 *         ),
 *         @OA\Property(property="created_at", type="string", format="date-time", description="Initial creation timestamp", readOnly="true"),
 *         @OA\Property(property="updated_at", type="string", format="date-time", description="Last update timestamp", readOnly="true")
 *     )
 * )
 *
 * Class Show
 */
class Show extends Model
{
    public function genres()
    {
        return $this->belongsToMany(Genre::class);
    }

    /**
     * Get long english language name
     *
     * @param [type] $value
     * @return void
     */
    public function getOriginalLanguageAttribute($value)
    {
        return Language::where('iso_639_1', $value)->first();
    }

    /**
     * Get formated first air date.
     *
     * @param [type] $value
     * @return void
     */
    public function getFirstAirDateAttribute($value)
    {
        return Carbon::parse($value)->format('d.m.Y.');
    }

    /**
     * Undocumented function
     *
     * @param array $result
     * @return boolean
     */
    public function saveShow(array $result)
    {
        $this->original_name = $result['original_name'];
        $this->name = $result['name'];
        $this->popularity = isset($result['popularity']) ? $result['popularity'] : 0.0;
        $this->origin_country = isset($result['origin_country'][0]) ? $result['origin_country'][0]: null;
        $this->vote_count = $result['vote_count'];
        $this->first_air_date = $result['first_air_date'];
        $this->backdrop_path = $result['backdrop_path'];
        $this->original_language = $result['original_language'];
        if (isset($result['id'])) {
            $this->id = $result['id'];
        }        
        $this->vote_average = $result['vote_average'];
        $this->overview = $result['overview'];
        $this->poster_path = $result['poster_path'];

        return $this->save();
    }
}
