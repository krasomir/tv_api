<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Alert extends Component
{
    public $errors;

    /**
     * Undocumented function
     *
     * @param object $errors
     */
    public function __construct(object $errors)
    {
        $this->errors = $errors;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.alert');
    }
}
