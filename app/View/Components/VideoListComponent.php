<?php

namespace App\View\Components;

use Illuminate\View\Component;

class VideoListComponent extends Component
{
    public $videoList;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($videoList)
    {
        $this->videoList = $videoList;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.video-list-component');
    }
}
