<header class="p-5">
    <div class="logo">
        <a href="{{ route('home') }}">
            <img src="{{ asset('images/tmdb.png') }}" alt="TMDB logo">
        </a>
    </div>
</header>