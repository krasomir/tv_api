<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('layouts.head')
    <body>
        @include('layouts.header')
        <div class="container mt-5">
            @yield('content')
        </div>
        @include('layouts.footer')
    </body>
</html>