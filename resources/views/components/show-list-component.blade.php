<div class="row">
    @foreach($showList as $show)
        <div class="col-sm-12 col-md-6 card-container">
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        @if($show->poster_path)
                            <img src="{{ env('TMDB_IMAGE_BASE_URL') . '/w154/' . $show->poster_path }}" class="card-img" alt="{{ $show->name }}">
                        @else
                            <img src="{{ asset('images/poster-placeholder.png') }}" alt="default image">
                        @endif
                        </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">
                                <a href="{{ route('show-details', ['id' => $show->id, 'slug' => Str::slug($show->name, '-')]) }}">
                                    {{ $show->name }}
                                </a>
                            </h5>
                            <p class="card-text">{{ Str::limit($show->overview, 200) }}</p>
                            <p class="card-text">
                                <small class="text-muted">{{ $show->first_air_date }}</small>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>