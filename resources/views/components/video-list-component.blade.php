<div class="row">
    @foreach($videoList as $video)
        <div class="col-sm-12 col-md-6 card-container">
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        @if($video->poster_path)
                            <img src="{{ env('TMDB_IMAGE_BASE_URL') . '/w154/' . $video->poster_path }}" class="card-img" alt="{{ $video->title }}">
                        @else
                            <img src="{{ asset('images/poster-placeholder.png') }}" alt="default image">
                        @endif
                        </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">
                                <a href="{{ route('details', ['id' => $video->id, 'slug' => Str::slug($video->title, '-')]) }}">
                                    {{ $video->title }}
                                </a>
                            </h5>
                            <p class="card-text">{{ Str::limit($video->overview, 200) }}</p>
                            <p class="card-text">
                                <small class="text-muted">{{ $video->release_date }}</small>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>