@extends('layouts.app')
@section('title', 'TMDB')

@section('content')
<form id="tvMovieSearchForm" method="POST" action="{{ route('search') }}">
    @csrf
    <div class="form-row">
        <div class="col-12 col-sm-4 mb-2">
            <input type="text" class="form-control" name="searchTerm" placeholder="find movie or show">
        </div>
        <div class="col-12 col-sm-4 mb-2">
            <select class="custom-select mr-sm-2" name="searchType" id="inlineFormCustomSelect">
                <option value="movie" selected>Movie</option>
                <option value="tv">TV Show</option>
            </select>
        </div>
        <div class="col-12 col-sm-4 mb-2">
            <button type="submit" class="btn btn-primary btn-block">Search</button>
        </div>
    </div>
</form>

@if($errors->any())
    <x-alert :errors="$errors" />
@endif

@if($searchResult)
<div id="searchResult" class="container mt-5 p-0">
    @if($searchType === 'movie')
        <!-- video list component -->
        <x-video-list-component :videoList="$searchResult" />
    @else
        <!-- show list component -->
        <x-show-list-component :showList="$searchResult" />
    @endif
</div>
@else
    @if(Route::currentRouteName() !== 'home')
        <div class="container mt-5 p-0">There is no result for that query.</div>
    @endif
@endif
@endsection