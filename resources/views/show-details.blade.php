@extends('layouts.app')
@section('title', 'TMDB')

@section('content')

<div class="container">
    <h1 class="mb-2 overflow-hidden">
        <span class="float-left">
            {{ $show->name }} <small class="font-weight-lighter">({{ \Carbon\Carbon::parse($show->first_air_date)->format('Y') }})</small>
        </span>
        <span class="small float-right">
            <a href="{{ route('home') }}">home</a>
        </span>
    </h1>
    <div class="backdrop-image">
        @if($show->backdrop_path)
            <img src="{{ env('TMDB_IMAGE_BASE_URL') . '/original/' . $show->backdrop_path }}" class="card-img" alt="{{ $show->name }}">
        @elseif($show->poster_path)
            <img src="{{ env('TMDB_IMAGE_BASE_URL') . '/original/' . $show->poster_path }}" class="card-img poster" alt="{{ $show->name }}">
        @else
            <img src="{{ asset('images/poster-placeholder.png') }}" alt="default image">
        @endif
    </div>
    <div class="jumbotron">
        <label class="d-block font-weight-bold">Overview:</label>
        <p>{{ $show->overview }}</p>
    </div>

    <!-- details -->
    <div class="container">
        <div class="row">
            <div class="col">
                <label class="d-block font-weight-bold">Number of seasons:</label>
                <p>{{ $details->number_of_seasons }}</p>
            </div>
            <div class="col">
                <label class="d-block font-weight-bold">Number of episodes:</label>
                <p>{{ $details->number_of_episodes }}</p>
            </div>
            <div class="col">
                <label class="d-block font-weight-bold">Genres:</label>
                <p>
                    @foreach($details->genres as $genre)
                    <span>{{ $genre->name }}</span>@if(!$loop->last), @endif
                    @endforeach
                </p>
            </div>
            <div class="col">
                <label class="d-block font-weight-bold">Language:</label>
                <p>{{ $show->original_language->english_name }}</p>
            </div>
            <div class="col">
                <label class="d-block font-weight-bold">Ratings:</label>
                <p>{{ $details->vote_average }}/10</p>
            </div>
        </div>
    </div>

    <!-- cast -->
    @if(count($credits->cast) > 0)
    <h5>CAST:</h5>
    <div class="cast-profile container">
        <div class="row">
        @foreach($credits->cast as $cast)
            @if($cast->id)
                <div class="card mb-3 col-sm-12 col-md-3 p-0" style="max-width: 540px;">
                    <div class="row no-gutters">
                        <div class="col-md-5 p-0">                            
                            @if($cast->profile_path)
                                <img src="{{ env('TMDB_IMAGE_BASE_URL') . '/w185/' . $cast->profile_path }}" class="card-img" alt="{{ $cast->name }}">
                            @else
                                <img src="{{ asset('images/poster-placeholder.png') }}" alt="default image" class="default-profile">
                            @endif
                        </div>
                        <div class="col-md-7">
                            <div class="card-body">
                                <h5 class="card-title">
                                    <a href="https://www.themoviedb.org/person/{{ $cast->id }}" target="_new">
                                        {{ $cast->name }}
                                    </a>
                                </h5>
                                <p class="card-text">{{ $cast->character }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
        </div>
    </div>
    @endif

    <!-- crew -->
    @if(count($credits->crew) > 0)
    <h5 id="crew-label" class="d-none">CREW:</h5>
    <div class="cast-profile container">
        <div class="row">
        @foreach($credits->crew as $crew)
            @if($crew->id && ($crew->job === 'Director' || $crew->job === 'Producer' || $crew->job === 'Executive Producer'))
                <div class="card mb-3 col-sm-12 col-md-3 p-0" style="max-width: 540px;">
                    <div class="row no-gutters">
                        <div class="col-md-5 p-0">                            
                            @if($crew->profile_path)
                                <img src="{{ env('TMDB_IMAGE_BASE_URL') . '/w185/' . $crew->profile_path }}" class="card-img" alt="{{ $crew->name }}">
                            @else
                                <img src="{{ asset('images/poster-placeholder.png') }}" alt="default image" class="default-profile">
                            @endif
                        </div>
                        <div class="col-md-7">
                            <div class="card-body">
                                <h5 class="card-title">
                                    <a href="https://www.themoviedb.org/person/{{ $crew->id }}" target="_new">
                                        {{ $crew->name }}
                                    </a>
                                </h5>
                                <p class="card-text">{{ $crew->job }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            @if($loop->last)
                <script>
                    document.getElementById('crew-label').classList.remove('d-none');
                </script>
            @endif
        @endforeach
        </div>
    </div>
    @endif

</div>

@endsection