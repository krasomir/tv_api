<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SearchController@index')->name('home');
Route::post('/search','SearchController@search')->name('search');
Route::get('/movie/{id}/{slug}','MovieController@showDetails')->name('details');
Route::get('/show/{id}/{slug}','ShowController@showDetails')->name('show-details');
