<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/movies/{name?}','Api\MovieController@index')->name('get-movies');
Route::get('/movie/{id}','Api\MovieController@show')->name('get-movie');
Route::post('/movie','Api\MovieController@store')->name('save-movie');

Route::get('/shows/{name?}','Api\ShowController@index')->name('get-shows');
Route::get('/show/{id}','Api\ShowController@show')->name('get-show');
Route::post('/show','Api\ShowController@store')->name('save-show');